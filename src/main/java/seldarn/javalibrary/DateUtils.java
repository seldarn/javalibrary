package seldarn.javalibrary;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Classe utilitaire de transformation de dates
 * @author Damien Boyaval
 *
 */
public class DateUtils {
	
	/**
	 * Transform Date to LocalDate
	 * @param date
	 * @return an object LocalDate
	 */
	public static LocalDate toLocalDate(Date date) {
		if (date == null) {
            return null;
        }
        if (date instanceof java.sql.Date) {
            return ((java.sql.Date)date).toLocalDate();
        } 
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	/**
	 * Transform LocalDate to Date
	 * @param localDate
	 * @return an object Date
	 */
	public static Date toDate(LocalDate localDate) {
		if (localDate == null){
            return null;
        } 
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	

}
