package seldarn.javalibrary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Test;

@DisplayNameGeneration(ReplaceUnderscores.class) 
public class TestDateUtils {
    @Test
    public void convert_date_to_localdate() throws ParseException{
        //GIVEN
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("28/12/1977");
       
        //WHEN
        LocalDate localDate = DateUtils.toLocalDate(date);

        //THEN
        assertEquals("1977-12-28", localDate.toString());
    }
     


}
